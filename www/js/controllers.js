angular.module('kl-ionic-app.controllers')

.controller('DashCtrl', function($scope,UserService, $firebaseObject, $firebaseArray) {
  //models
  var vm = this;
  vm.inputSearch = '';
  vm.viewTitle = "My Friends";
  vm.viewTitle = "My Friends";
  vm.onButtonClick = function(){
    vm.viewTitle = "New View Title";
  };
  UserService.getUsersFromAPI().then(function(users){
    console.log(users);
  },function(err){
    alert(err);
  });

  $scope.showList = false;

  $scope.toggleListShow = function(){
    $scope.showList = !$scope.showList;
  };

  var ref = firebase.database().ref();
  var childRef = ref.child('/items');
  $scope.data = $firebaseObject(ref);
  $scope.$items =   $firebaseArray(childRef);
  $scope.$items.$add({
    text: "Test_" + new Date().getTime()
  });
  console.log("items", $scope.data);
  vm.usersJSON = UserService.getUsers();
  UserService.updateFirstUser("AhsanAyaz");
  vm.usersJSON[0].name = "AhsanAyaz";
})


.directive('unless',function(){
  var directiveDefinitionObject = {
    restrict: "EACM",
    scope: {
      showUnless: "=myVar"
    },
    link: function(scope, elem, atr){
      console.log("inside link function");
      console.log(scope.showUnless);
      scope.$watch('showUnless', function(newVal,oldVal){
        console.log('oldVal',oldVal);
        console.log('newVal',newVal);
        if(!newVal){ //if newVal is true
          angular.element(elem).addClass('ng-hide');
        }
        else{
          angular.element(elem).removeClass('ng-hide');
        }
      });
    },
    controller: function($scope){
      console.log("inside controller");
      console.log($scope.showUnless);
    },
    controllerAs: ''
  };


  return directiveDefinitionObject;
})


.controller('ChatsCtrl', function($scope, Chats, UserService) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.users = UserService.getUsers();
  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
