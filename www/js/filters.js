/**
 * Created by ahsanayaz on 09/01/2017.
 */


(function () {
  'use strict';

  angular
    .module('kl-ionic-app.filters')
    .filter('customLimit', customLimit);

  function customLimit() {
    return customLimitFilter;

    ////////////////

    function customLimitFilter(itemString, limit, something) {
      console.log(something);
      if(itemString.length > limit){
        var newStr = itemString.substring(0, limit);
        newStr += "...";
        return newStr;
      }
      return itemString;
    }
  }

})();

(function () {
  'use strict';

  angular
    .module('kl-ionic-app.filters')
    .filter('limitArr', limitArr);

  function limitArr() {
    return limitArrFilter;

    ////////////////

    function limitArrFilter(arr, limit) {
      limit = limit ? limit : 5;
      var newArr = [];
      if(arr.length>limit){
        for(var i=0,l=limit ; i<l ;i++){
          newArr.push(arr[i]);
        }
      }
      else{
        newArr = arr;
      }

      return newArr;
    }
  }

})();


